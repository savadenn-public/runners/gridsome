FROM node:13

RUN apt update && apt install -y libvips libvips-dev libvips-tools

RUN yarn global add gridsome node-sass sharp
