build:
	docker build -t savadenn-public/runners/gridsome .

test: build
	@docker run --rm -it savadenn-public/runners/gridsome bash
